#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import codecs

verbs = set()
nouns = set()
adverbs = set()

for line in codecs.open('spanish.txt.learn', encoding = 'cp1252'):
    fields = line.split('\t')
    if (fields[2].strip() == 'V'):
        verbs.add(fields[1])
    elif (fields[2].strip() == 'N'):
        nouns.add(fields[1])
    elif (fields[2].strip() == 'A'):
        adverbs.add(fields[1])

Arules = [
        '+A:0'
        ]

Vrules = [
        '+V:0',

        '+V+Pres+1P+Un:^o',
        '+V+Pres+2P+Un:^s',
        '+V+Pres+3P+Un:^$',
        '+V+Pres+1P+Pl:^mos',
        '+V+Pres+2P+Pl:^*s',
        '+V+Pres+3P+Pl:^n',
        
        '+V+PretImperf+1P+Un:^a',
        '+V+PretImperf+2P+Un:^as',

        '+V+PretInd+1P+Un:^*',
        '+V+PretInd+2P+Un:^ste',
        u'+V+PretInd+3P+Un:^ó',
        '+V+PretInd+1P+Pl:^$mos',
        '+V+PretInd+2P+Pl:^steis',
        '+V+PretInd+3P+Pl:^ron',

        '+V+PretPlus:^ado'

        ]

Nrules = [
        '+N:0'
        ]


lexc = codecs.open('spanish.lexc', encoding = 'utf-8', mode = 'w+')

lexc.write('Multichar_Symbols +N +V +A +1P +2P +3P +Pl +Un +Pres +PretImperf +PretInd +FutImperf +PretPerf +PretPlus +PretAnt +FutPerf \n')

lexc.write("LEXICON Root\n")
lexc.write("Verb ;\n")
lexc.write("Noun ;\n")
lexc.write("Adv ;\n")

def writeVords(vords, clas):
    for vord in vords:
        lexc.write('%s\t%sinf;\n' % (vord, clas))

def writeRules(rules):
    for rule in rules:
        lexc.write('%s\t#;\n' %  rule)


lexc.write("\nLEXICON Verb\n")
writeVords(verbs, 'V')

lexc.write("\nLEXICON Noun\n")
writeVords(nouns, 'N')

lexc.write("\nLEXICON Adv\n")
writeVords(adverbs, 'A')

lexc.write("\nLEXICON Vinf\n")
writeRules(Vrules)

lexc.write('\nLEXICON Ainf\n')
writeRules(Arules)

lexc.write('\nLEXICON Ninf\n')
writeRules(Nrules)
